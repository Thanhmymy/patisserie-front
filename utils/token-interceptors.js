import axios from "axios";
import { getSession } from "next-auth/react";

const api = axios.create({
	baseURL: process.env.NEXT_PUBLIC_API_URL
})
api.interceptors.response.use((response) => {

	return response
}, (error) => {
	
	
	throw new Error(error.response)
})
api.interceptors.request.use(async (config) => {
	
	const session = await getSession();
	if (session) {

		config.headers.authorization = 'bearer ' + session.accessToken;
	}
	return config;
});



export default api
