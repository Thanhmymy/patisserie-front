import api from "./token-interceptors"

export async function fetchCommentFromProduct(product_id){
    const response = await api.get(process.env.NEXT_PUBLIC_API_URL +'/api/comment/all/' + product_id)
    console.log(response)
    return response.data
}

export async function fetchComment(){
    const response = await api.get(process.env.NEXT_PUBLIC_API_URL + '/api/comment')

    return response.data

}