import axios from "axios";
import api from "./token-interceptors";

export async function fetchIngredienFromProduct(product_id){
    const response = await api.get(process.env.NEXT_PUBLIC_API_URL +'/api/ingredient/all/' + product_id)
    console.log(response)
    return response.data
}

export async function fetchIngredient(){
    const response = await api.get(process.env.NEXT_PUBLIC_API_URL + '/api/ingredient')

    return response.data

}