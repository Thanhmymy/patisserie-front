import axios from 'axios'
import api from './token-interceptors'

export async function fetchProductId(id){
    const response = await api.get(process.env.NEXT_PUBLIC_API_URL + '/api/product/' + id)

    return response.data

}

export async function addProduct(){
    const response = await api.get(process.env.NEXT_PUBLIC_API_URL + '/api/product/add')
    return response.data
}

export async function fetchProductByCategory(){
    const response = await api.get(process.env.NEXT_PUBLIC_API_URL + '/api/category/all' + id)
    return response.data
}