import api from "./token-interceptors"

export async function fetchCategoryId(id){
    const response = await api.get(process.env.NEXT_PUBLIC_API_URL + '/api/category/' + id)

    return response.data

}
export async function fetchProductByCategory(id){
    const response = await api.get(process.env.NEXT_PUBLIC_API_URL + '/api/category/all' + id)
    return response.data
}