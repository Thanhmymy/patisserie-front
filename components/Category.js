import Link from "next/link";

function Category({ id, name, products }) {



    return (
        <div className="mt-5">
            <Link href={'/category/' + id}>
                <button className="border rounded p-2 bg-[#BA9F87] border-white text-white">{name}</button>
                

            </Link>
        </div>
    )
}

export default Category;