import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useState } from "react";
import api from "../utils/token-interceptors";


function PersonalForm({ onSubmit, ingredient }) {

    const [ingredients, setIngredients] = useState({
        ingredientId: "",
        quantity: "",
        mesure: ""
    })
    const [finalIngredient, setFinalIngredient] = useState([])
    const { data: session } = useSession()
    const user = session?.user
    console.log(finalIngredient);
    const initial = {
        name: '',
        description: '',
        date: '',
        image: '',
        category_id: '',
        ingredient: []
    }
    const [form, setForm] = useState(initial)

    const handleChangeIngredient = (event) => {
        setIngredients({
            ...ingredients,
            [event.target.name]: event.target.value
        });
    }
    const handleSubmitIngredient = (event) => {
        event.preventDefault();
        if (ingredients && ingredients.ingredientId && ingredients.quantity) {
            setFinalIngredient([...finalIngredient, ingredients])
            setIngredients({
                ingredientId: "",
                quantity: "",
                mesure: ""
            })

        }
    }

    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }
    const handleSubmit = (event) => {

        event.preventDefault();
        if (form && finalIngredient.length) {
            onSubmit({ ...form, ingredient: finalIngredient });
        }
    }


    return (
        <>
            {session && user.role == 'admin' &&
                <>
                    <div className="flex flex-col lg:flex-row justify-around items-center border-b  border-black">
                        <h1 className="text-xl mt-5">Formulaire d'ajout</h1>
                        <div className="p-2 mb-5">
                            <div className="w-full max-w-xs">
                                <form class="bg-white shadow-md mt-5 rounded px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit}>
                                    <div className="flex">
                                        <div className="mb-4">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" for="name">
                                                Name
                                            </label>
                                            <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="Name" type="text" onChange={handleChange} name="name" value={form.name} required />
                                        </div>
                                        <div class="mb-6">
                                            <label className="block text-gray-700 text-sm font-bold mb-2" for="password">
                                                Description
                                            </label>
                                            <input className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" placeholder="Description" type="text" onChange={handleChange} name="description" value={form.description} required />
                                        </div>
                                    </div>

                                    <div class="mb-6">
                                        <label className="block text-gray-700 text-sm font-bold mb-2" for="date">
                                            Date
                                        </label>
                                        <input className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" placeholder="date" type="date" onChange={handleChange} name="date" value={form.date} required />
                                    </div>

                                    <div class="mb-6">
                                        <label className="block text-gray-700 text-sm font-bold mb-2" for="image">
                                            Image
                                        </label>
                                        <input className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" placeholder="Image" type="text" onChange={handleChange} name="image" value={form.image} required />
                                    </div>
                                    <div class="mb-6">
                                        <label className="block text-gray-700 text-sm font-bold mb-2" for="category_id">
                                            Category_id
                                        </label>
                                        <input className="shadow appearance-none border  rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" placeholder="Category_id" type="number" onChange={handleChange} name="category_id" value={form.category_id} required />
                                    </div>
                                    <div class="flex items-center justify-between">
                                        <button className="bg-[#ba9f87] text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                                            Ajouter
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    {/*Formulaire Ingredient*/}
                    <form onSubmit={handleSubmitIngredient}>
                        <select name="ingredientId" value={ingredients.ingredientId} id="Ingredient" onChange={handleChangeIngredient}>
                            <option value="">--Please choose an option--</option>
                            {ingredient.map((item) => {
                                return (
                                    <>

                                        <option value={item.id} >
                                            {item.name}
                                        </option>

                                    </>

                                )

                            })}
                        </select>
                        <input className="border border-black" value={ingredients.quantity} name="quantity" type="number" onChange={handleChangeIngredient} />
                        <select name="mesure" value={ingredients.mesure} id="mesure" onChange={handleChangeIngredient}>
                            <option value="g">
                                Grammes
                            </option>
                            <option value="L">
                                Litres
                            </option>
                            <option value="">
                                Rien
                            </option>
                        </select>
                        <button className="border border-black">Submit</button>
                    </form>
                    <ul>{
                        finalIngredient.length ? finalIngredient.map(item => {
                            return <li>
                                <button className="mx-2 my-1 border bg-red-700 text-white px-2" onClick={() => setFinalIngredient(finalIngredient.filter(element => element.ingredientId !== item.ingredientId))}>X</button>
                                {ingredient.find(element => element.id === Number(item.ingredientId)).name + " : " + item.quantity + item.mesure}
                            </li>
                        }) : null
                    }</ul>
                </>

            }

        </>

    )
}


export default PersonalForm;

