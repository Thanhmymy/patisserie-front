function Footer() {
    return (
        <>
            <div>
                <p className="text-center text-lg lg:text-xl text-white">Du lundi au vendredi 9h30-12h30 13h30-17h30 </p>
                <p className="text-center text-lg lg:text-xl text-white">Du samedi au dimanche 9h-12h </p>
            </div>
            <div className="flex justify-around mt-10">
                <p className="text-sm text-white">Mentions Légales</p>
                <p className="text-sm text-white">Mentions Légales</p>
                <p className="text-sm text-white">Mention Légales</p>
            </div>
            <p className="text-center mt-5 text-white">2022 Par Thanhmy.</p>
        </>

    )
}

export default Footer;  