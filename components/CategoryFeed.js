import Category from "./Category";


function CategoryFeed({ categories, products }) {
    

    return (

        <div className="flex justify-around">

            {categories.slice(0).map(({ id, name }) => (
                <>
                    <Category
                        key={id}
                        id={id}
                        name={name}
                        products={products}
                    />
                </>
            ))}
        </div>
    )
}

export default CategoryFeed;