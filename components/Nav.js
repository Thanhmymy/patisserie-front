import { signOut, useSession, signIn } from "next-auth/react";
import { useState } from "react";


function Header() {
    const { data: session } = useSession()
    const [showLinks, setShowLinks] = useState(false);
    const handleShowLinks = () => {
        setShowLinks(!showLinks)
    }
    return <div>
        <nav className={`navbar ${showLinks ? "show-nav" : "hide-nav"} `}>
            <div className="navbar_logo"><a href="/">
                <img className="h-20 lg:h-32" src="/Assets/FVY.png" alt="" />
            </a></div>
            <ul className="navbar_links">
                <a className="navbar_link" href="/blog">
                    <li className="navbar_item lg:text-xl">
                        Blog
                    </li>
                </a>
                <a className="navbar_link" href="/contact">
                    <li className="navbar_item lg:text-xl">
                        Contact
                    </li>
                </a>
                {!session &&

                        <img onClick={() => signIn()} src="/Assets/user.png" className="h-7" alt="" />

                }
                {session && 
                <button type="button" className="border-2 border-black" onClick={() => signOut()}>Logout</button>
                }

            </ul>
            <button className="navbar_burger" onClick={handleShowLinks}>
                <span className="burger-bar"></span>
            </button>
        </nav>
    </div>
}

export default Header;