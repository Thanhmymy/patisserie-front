import Product from "./Product";

function ProductFeed({products, onDelete}){
    return(
        <div className="grid grid-flow-row-dense lg:grid-cols-4">
            {products.slice(0).map(({id,name,description,image,comment_id,category_id})=>(
                <>
                <Product
                key={id}
                id={id}
                name={name}
                description={description}
                image={image}
                comment_id={comment_id}
                category_id={category_id}
                onDelete={onDelete}/>

                </>

            ))
            }
            
        </div>
    )
}

export default ProductFeed;