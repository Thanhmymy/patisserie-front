import { useSession } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";

function Product({ id, name, description, image, comment_id, category_id, onDelete }) {
    const { data: session } = useSession()
    const user = session?.user
    return (



        <div className="p-2 m-5 ">

            <div class="max-w-xs rounded overflow-hidden shadow-lg">
                <Image height={350} width={350} src={image} alt="Sunset in the mountains" />
                <div class="px-6 py-4">
                    <div class="font-bold text-xl mb-2">{name}</div>
                    <p class="text-gray-700 text-base">
                        {description}
                    </p>
                    <div className="flex justify-around">                    
                        {session && user.role == 'admin' &&
                        <button className="border w-2/3 bg-gray-300 mt-5" onClick={() => onDelete(id)}>Supprimer</button>
                    }
                        <Link href={'/blog/' + id}>
                            <button className="border w-1/3 bg-gray-300 mt-5">Voir</button>
                        </Link>

                    </div>



                </div>

            </div>
        </div>
    )

}

export default Product;