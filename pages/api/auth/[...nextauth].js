import axios from "axios";
import NextAuth from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials"

export default NextAuth({
    jwt:{secret:process.env.JWT_SECRET},
    callbacks: {
        async jwt({ token, user }) {
            if (user) {
                return {
                    accessToken: user.token,
                    user: user.user,
                }
            }
            

            return token;
        },
        async session({ session, token }) {
            session.accessToken = token.accessToken;
            session.user = token.user;
            return session;
        }
    },
    providers: [
        CredentialsProvider({
            name: "Login",
            credentials: {
                email: { label: "Email", type: "email", placeholder: "Your email..." },
                password: { label: "Password", type: "password", placeholder: "Your Password..." }
            },
            async authorize(credentials, req) {
                try {
                    const response = await axios.post("http://localhost:8000/api/user/login", credentials);
                    const data = response.data;
                    if (data.user && data.token) {
                        return data;
                    }
                    return null;
                } catch (error) {
                    console.log("error");
                    return null;
                }
            }
        }),
        CredentialsProvider({
            name: "Sign-Up",
            type: "credentials",
            id: "signup",
            credentials: {
                name: { label: "Name", type: 'text' },
                email: { label: "Email", type: "text", placeholder: "Exemple : test@gmail.com" },
                password: { label: "Password", type: "password" },
            },
            authorize: async (credentials) => {
                try {
                    const { data } = await axios.post("http://localhost:8000/api/user/register", {
                        name: credentials?.name,
                        email: credentials?.email,
                        password: credentials?.password,
                    })
                    
                    if (data) {
                        return { status: 'success', data: data }

                    } else {
                        return data
                    }
                } catch (e) {

                    throw new Error(e.response.data.error)

                }

            }
        }), 
    ],

})
