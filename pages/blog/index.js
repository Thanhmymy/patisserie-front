import axios from "axios";
import ProductFeed from "../../components/ProductFeed";
import Nav from '../../components/Nav'
import api from "../../utils/token-interceptors";
import { addProduct } from "../../utils/product-services";
import { useState } from "react";
import PersonalForm from "../../components/Personal-form";
import CategoryFeed from "../../components/CategoryFeed";
import Footer from "../../components/Footer";

function blog({ products, categories, ingredient }) {


    const [posts, setPost] = useState(products);

    const addPost = async (post) => {
        try {
            const add = await api.post(process.env.NEXT_PUBLIC_API_URL + '/api/product/add', post)
            setPost([
                ...posts,
                add
            ]);
        } catch (error) {
            return error;
        }

    }

    async function deleteProduct(id) {
        await api.delete(process.env.NEXT_PUBLIC_API_URL + '/api/product/' + id);
        setPost(
            posts.filter(item => item.id !== id)
        )
    }
    //function category


    return <div className="">
        <Nav />
        <div className="bg-[#ba9f87] p-2">
            <h1 className="text-center text-white text-2xl mb-10 mt-10">Les patisseries</h1>


            <div className="bg-white lg:m-10">

                <PersonalForm ingredient={ingredient} onSubmit={addPost} />

                <CategoryFeed categories={categories} products={products} />
                <ProductFeed products={posts} onDelete={deleteProduct} />


            </div>
            <Footer />
        </div>

    </div>
}



export async function getServerSideProps(context) {
    const productsProp = await api.get(process.env.NEXT_PUBLIC_API_URL + "/api/product");
    const categoryProp = await api.get(process.env.NEXT_PUBLIC_API_URL + "/api/category");
    const ingredientProp = await api.get(process.env.NEXT_PUBLIC_API_URL + "/api/ingredient")

    // console.log(productsProp)
    return {
        props: {
            products: productsProp.data,
            categories: categoryProp.data,
            ingredient : ingredientProp.data,
        }

    }

}



export default blog;