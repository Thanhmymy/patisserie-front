import Image from "next/image";
import { fetchProductId } from "../../utils/product-services";
import Nav from "../../components/Nav"
import { useState } from "react";
import api from "../../utils/token-interceptors";
import { fetchComment } from "../../utils/comment-services";
import { signIn, useSession } from "next-auth/react";
import { fetchIngredient } from "../../utils/ingredient-services";

function productID({ product, comment, ingredient }) {
    const [value, setValue] = useState(1)
    const { data: session } = useSession()
    const user = session?.user

    const initial = {
        message: '',
    }

    const [form, setForm] = useState(initial)
    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }
    const handleSubmit = (event) => {
        event.preventDefault();
        addComment(form);
    }

    const [comments, setComments] = useState(comment);

    const addComment = async (comment) => {
        try {
            const add = await api.post(process.env.NEXT_PUBLIC_API_URL + '/api/comment', { ...comment, product_id: product.id })
            setPost([
                ...comments,
                add
            ]);
        } catch (error) {
            return error;
        }

    }

    // async function deleteComment(id) {
    //     await api.delete(process.env.NEXT_PUBLIC_API_URL + '/api/comment/' + id);
    //     setComments(
    //         comments.filter(item => item.id !== id)
    //     )
    // }
    // console.log(comment);
    console.log(ingredient);
    return (
        <div>
            <Nav />
            <div className="bg-[#ba9f87] p-5">
                <div className="bg-white">
                    <div className=" flex items-center lg:p-20 justify-around pt-5 lg:grid lg:grid-cols-2">
                        <img src={product.image} className="pl-5 w-1/2 lg:h-80" />
                        <div className="flex flex-col">
                            <h1 className="text-xl border-b-2 border-black">{product.name}</h1>
                            <p>{product.description}</p>

                        </div>

                    </div>

                    <h1 className="text-center mt-5 text-xl">Les ingrédients</h1>
                    <div className="flex items-center justify-center mt-5">
                        <button className="hover:bg-gray-300 border border-black rounded-full text-2xl p-1" onClick={() => setValue(value += 1)}>+</button>
                        <p className="text-xl m-2">{value} personnes</p>
                        <button disabled={value <= 1} className="hover:bg-gray-300 border border-black rounded-full text-3xl p-1" onClick={() => setValue(value -= 1)}>-</button>
                    </div>
                    <div className="grid grid-cols-2 grid-flow-row-dense lg:grid-cols-4">
                        {product.ingredient.length ? product.ingredient.map((item) => {
                            return (
                                <div key={item.id} className="flex flex-col items-center mt-10 ">
                                    <p className="text-xl border-b-2 border-black mb-2">{item.name}</p>
                                    <Image className="" src={item.image} width={100} height={100} />
                                    <p className="mt-2">{item.quantity * value}{item.mesure}</p>
                                </div>
                            )
                        }) : null}
                    </div>


                    {/*Comment*/}

                    {user &&
                        <>
                            <h2 className="mt-10 ml-5">Poster un commentaire ?</h2>
                            <form action="" className="m-5" onSubmit={handleSubmit}>
                                <textarea className="border w-full p-5" name="message" id="message" cols="30" rows="10" onChange={handleChange}>
                                </textarea>

                                <button className="border border-black m-5 p-2">Submit</button>
                            </form>
                        </>

                    }
                    {!user &&
                        <div className="flex items-center m-5">
                            <h1 className="ml-10">Se connecter pour pouvoir commenter {" =>"}</h1>
                            <button onClick={() => signIn()} className="border ml-5 p-2">Sign in/Sign up?</button>
                        </div>

                    }

                    <div className="border p-2">
                        <h1>Commentaires:</h1>
                        {product.comment.map((commentId) => {
                            console.log(commentId);
                            return (
                                <div key={commentId.id}>
                                    <div className="border p-2 m-2 border-black">
                                        <p className="border-b border-black mb-5 font-bold"> {commentId.user_id.name}</p>
                                        <p>{commentId.message}</p>
                                    </div>


                                </div>
                            )
                        })}
                    </div>

                </div>

            </div>

        </div>
    )
}

export async function getServerSideProps(context) {
    if (context.query.id) {

        return {
            props: {
                product: await fetchProductId(context.query.id),
                comment: await fetchComment(),
                ingredient: await fetchIngredient()
            }
        }
    } return {
        notFound: true
    }

}


export default productID;