import Image from "next/image";
import Link from "next/link";
import { fetchCategoryId, fetchProductByCategory } from "../../utils/category-services";
import Nav from '../../components/Nav'

function categoryId({ category }) {
    return (

        <>

            <Nav />
            <div className="bg-[#ba9f87] p-2 mx-auto ml-auto">

                <h1 className="text-center text-white text-2xl mb-10 mt-10">Les patisseries {category.name}</h1>
                <div className="bg-white lg:m-12">
                    <Link href="/blog">
                    <img src="/Assets/left-arrow.png" className="h-14 cursor-pointer border p-2 m-5 " alt="" />
                    </Link>


                    <div className="grid grid-flow-row-dense lg:grid-cols-4">
                        {category.product.map((categoryId) => {

                            return (

                                <>

                                    <div className="p-2 m-5 ">
                                        <div key={categoryId.id} class="max-w-xs rounded overflow-hidden shadow-lg">
                                            <Image height={350} width={350} src={categoryId.image} alt="Sunset in the mountains" />
                                            <div class="px-6 py-4">
                                                <div class="font-bold text-xl mb-2">{categoryId.name}</div>
                                                <p class="text-gray-700 text-base">
                                                    {categoryId.description}
                                                </p>
                                                <Link href={'/blog/' + categoryId.id}>
                                                    <button className="border w-1/3 bg-gray-300 mt-5">Voir</button>
                                                </Link>
                                            </div>

                                        </div>
                                    </div>

                                </>

                            )

                        })}
                    </div>
                </div>
            </div>
        </>
    )



}
export async function getServerSideProps(context) {
    if (context.query.id) {

        return {
            props: {
                category: await fetchCategoryId(context.query.id)
                // category: await fetchProductByCategory(context.query.id)
            }
        }
    } return {
        notFound: true
    }

}

export default categoryId;