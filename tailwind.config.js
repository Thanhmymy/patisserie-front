module.exports = {
  mode : 'jit',
  content: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      backgroundImage: theme=>({
        'banniere': "url('/Assets/banniere.png')"
      })
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
